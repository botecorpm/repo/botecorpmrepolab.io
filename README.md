# Botecopackagers.Gitlab.Io
<div align="center">

#### Seja muito bem-vindo a [botecopackagers.gitlab.io](https://botecopackagers.gitlab.io/) o repositório de desenvolvimento do [botecorpm.gitlab.io](https://botecorpm.gitlab.io/)

<p align="center">
  <img src="logo.png" width="230" />
</p>
</div>

#### Introdução
BotecoRPM é um projeto social educativo, pretende coletar, unificar, aprimorar, facilitar e disponibilizar de maneira intuitiva recursos sobre empacotamento RPM, a fim de ser uma ferramenta de aprendizado fácil acesso e compreensão.
Também de servir como repositório de pacotes para diversas distros que usam RPM, e principalmente apoiando futuros profissionais de Tecnologia da Informação possibilitando a troca de experiência e ajuda. 


# Projetos Open-Source para contribuir

Espaço para a divulgação de projetos.


| Nome | Site | Outros links |
| --- | --- | --- |


```





